import org.scalatest._
import interview.MainEjercicioCinco

class MainEjercicioCincoSpec extends FlatSpec with Matchers {
  "MainEjercicioCinco" should 
    "Get all the base anagrams from the list" in {
      val base = "amor"
      val list = Seq("roma", "ramo", "mora", "aroma", "arbol", "rmoa")
      MainEjercicioCinco.pickAnagrams(base, list) should === (Seq("roma", "ramo", "mora", "rmoa"))
    }
}
