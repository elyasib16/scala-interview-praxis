import org.scalatest._
import interview.MainEjercicioTres

class MainEjercicioTresSpec extends FlatSpec with Matchers {
  "MainEjercicioTres" should 
    "Succed" in {
      val expression = "(if (zero? x) max (/ 1 x))?".toList
      MainEjercicioTres.balanceo(expression) should === (true)
    }

  "MainEjercicioTres" should 
    "Fail" in {
      val expression = "())(".toList
      MainEjercicioTres.balanceo(expression) should === (false)
    }

  "MainEjercicioTres" should 
    "Also fail" in {
      val expression = "())".toList
      MainEjercicioTres.balanceo(expression) should === (false)
    }

  "MainEjercicioTres" should 
    "Fail again" in {
      val expression = "())a".toList
      MainEjercicioTres.balanceo(expression) should === (false)
    }
}
