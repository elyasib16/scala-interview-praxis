import org.scalatest._
import interview.MainEjercicioCuatro

class MainEjercicioCuatroSpec extends FlatSpec with Matchers {
  "MainEjercicioCuatro" should 
    "Get all the combinations possible" in {
      val amount = 4
      val coins = List(1, 2)
      MainEjercicioCuatro.conteoCambio(amount, coins) should === (3)
    }

  "MainEjercicioCuatro" should 
    "Also get all the combinations possible" in {
      val amount = 8
      val coins = List(1, 2)
      MainEjercicioCuatro.conteoCambio(amount, coins) should === (5)
    }
}
