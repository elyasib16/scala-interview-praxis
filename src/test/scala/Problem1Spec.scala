import org.scalatest._
import interview.MainEjercicioUno

class MainEjercicioUnoSpec extends FlatSpec with Matchers {
  "MainEjercicioUno" should 
    "Cumpute the sum of all the elements of a list" in {
      val list = List(1, 2, 3, 4)
      val expected= list.sum
      MainEjercicioUno.sum(list) should === (expected)
    }

  "MainEjercicioUno" should 
    "Find the max element among all the elements of a list" in {
      val list = List(1, 2, 3, 4, 5 , -5, 0, 4)
      val expected= list.max
      MainEjercicioUno.max(list) should === (expected)
    }
}
