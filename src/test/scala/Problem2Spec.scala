import org.scalatest._
import interview.MainEjercicioDos

class MainEjercicioDosSpec extends FlatSpec with Matchers {
  "MainEjercicioDos" should 
    "Cumpute a random value from a pascal triangle" in {
      val row = 17
      val column = 4
      val expected = 2380
      MainEjercicioDos.pascal(column, row) should === (expected)
    }

  "MainEjercicioDos" should 
    "Cumpute another random value from a pascal triangle" in {
      val row = 10
      val column = 4 
      val expected = 210
      MainEjercicioDos.pascal(column, row) should === (expected)
    }
}
