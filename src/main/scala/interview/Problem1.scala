package interview

import annotation.tailrec

/* DESCRIPCIÓN DEL PROBLEMA
 *
 * Implementar métodos sum y max utilizando funciones recursivas
 */

/*
 * ::::::::::::::: SOLUCIÓN ::::::::::::::::
 * Irving Elyasib Santiago Catzim
 */

object MainEjercicioUno {
  def sum(xs: List[Int]): Int = {
    // auxSum es tailrecursive para evitar stack overflows
    @tailrec def auxSum(xs: List[Int], total: Int = 0): Int = {
      xs match {
        case Nil => total
        case head :: tail => auxSum(tail, head + total)
      }
    }
    auxSum(xs)
  }

  def max(xs: List[Int]): Int = {
    if (xs.isEmpty) throw new NoSuchElementException

    // auxMax es tailrecursive para evitar stack overflows
    @tailrec def auxMax(xs: List[Int], max: Int = Int.MinValue): Int = {
      xs match {
        case Nil => max
        case head :: tail =>
          val currentMax = if (head > max) head else max
          auxMax(tail, currentMax)
      }
    }

    auxMax(xs)
  }

}
