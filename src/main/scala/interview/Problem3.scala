package interview

import annotation.tailrec

/* DESCRIPCIÓN DEL PROBLEMA
 *
 * Balanceo de Paréntesis. Escribe una función que verifique el balanceo de paréntesis en una cadena, la cual es representada como una lista de tipo List[Char] y no de tipo String. 
 * Por ejemplo, la función debe regresar ‘true’  para las siguientes cadenas:
 *     
 *     1 (if (zero? x) max (/ 1 x))
 *     2 Te dije (que (aun) no está listo). (Pero no escuchas)
 *   
 *   Debe regresar ‘false’ para las siguientes cadenas:
 * 
 *     1 :-)
 *     2 ())(
 * 
 *   El último ejemplo demuestra que no es suficiente verificar que una cadena contiene el mismo 
 *   número de paréntesis tanto abiertos como cerrados.
 * 
 *   Realiza este ejercicio implementando la función balanceo de la clase MainEjercicioTres.scala
 *   Tip:  Puedes crear una función inner si necesitas pasar parámetros extra a tu función.
 *     Hay tres métodos en List[Char] que te servirán para este ejercicio:
 *   
 * chars.isEmpty:Boolean regresa true cuando la lista es vacía
 * chars.head: Char regresa el primer elemento de la lista
 * chars.tail: List[Char] regresa la lista sin el primer elemento
 */

/*
 * ::::::::::::::: SOLUCIÓN ::::::::::::::::
 * Irving Elyasib Santiago Catzim
 */

object MainEjercicioTres {

  def balanceo(chars: List[Char]): Boolean = {
    // auxSum es tailrecursive para evitar stack overflows
    @tailrec def auxBalanceo(chars: List[Char], open: Int): Boolean =
      chars match {
        case Nil if open == 0 =>
          true
        case '(' :: tail =>
          auxBalanceo(tail, open + 1)
        case ')' :: tail if open > 0 =>
          auxBalanceo(chars.tail, open - 1)
        case head :: tail if head != ')' =>
          auxBalanceo(tail, open)
        case _ =>
          false
      }
    auxBalanceo(chars, 0)
  }

}
  
