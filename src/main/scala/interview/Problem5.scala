package interview

import annotation.tailrec

/* DESCRIPCIÓN DEL PROBLEMA
 *
 * Dado un arreglo de cadenas, llamado ANAGRAMAS, que sólo contienen letras minúsculas y
 * un objeto tipo val llamado BASE, el cual es un arreglo de caracteres todos en minúsculas,
 * determinar cuáles de las cadenas en ANAGRAMAS en efecto son anagramas de BASE.
 * Escribe un código en scala que tenga un objeto singleton que resuelva el problema planteado
 * y que use una función de orden superior que analice si cada cadena en el arreglo es o no anagrama de la BASE.
 *
 * Ejemplo : BASE = amor, ANAGRAMAS : roma, ramo, mora, aroma, arbol, rmoa
 * resultado esperado : roma, ramo, mora, rmoa son anagramas pues usan todas las letras de amor
 */

/*
 * ::::::::::::::: SOLUCIÓN ::::::::::::::::
 * Irving Elyasib Santiago Catzim
 */

object MainEjercicioCinco {
  def getOcurrences(word: String) = word.foldLeft(Map[Char, Int]()){ (map, char) =>
      map + (char -> (map.get(char).getOrElse(0) + 1))
    }

  def pickAnagrams(base: String, anagrams: Seq[String]): Seq[String] = {
    val baseOcurrences = getOcurrences(base)

    anagrams.map(word => (word -> getOcurrences(word)))
      .collect{ case (word, ocurrences) if baseOcurrences == ocurrences => word }
  }
}

