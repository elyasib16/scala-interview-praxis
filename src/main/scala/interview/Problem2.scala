package interview

import annotation.tailrec
import collection.mutable.ListBuffer

/* DESCRIPCIÓN DEL PROBLEMA
 *
 * Triangulo de Pascal. El siguiente patrón se le conoce como triangulo de pascal.
 * 
 *     1
 *    1 1
 *   1 2 1
 *  1 3 3 1
 * 1 4 6 4 1
 *    ...
 * 
 * Todos los números al borde del triángulo son “1”, y cada número dentro del triángulo es la 
 * suma de sus dos números de arriba. Escribe la función pascal (marcada con  ??? ) que calcule 
 * los elementos del triángulo de Pascal de la clase MainEjercicioDos.scala
 */

/*
 * ::::::::::::::: SOLUCIÓN ::::::::::::::::
 * Irving Elyasib Santiago Catzim
 * El problema puede resolverse usando una función "tail recursive".
 * Además usando técnicas "memoization" puede eficientarse bastante el
 * cálculo.
 */

object MainEjercicioDos {
  def main(args: Array[String]) {
    println("Triangulo Pascal")
    for (row <- 0 to 10) {
      for (col <- 0 to row) print(pascal(col, row) + " ")
      println()
    }
  }

  // Esta estructura es usado como una optimización de "memoization"
  val triangle = ListBuffer(Seq(1));
  val zero = Seq[Int](1)

  def pascal(c: Int, r: Int): Int = {
    // Si la row ya existe, toma el valor de triangle
    if (triangle.size - 1 >= r) {
      triangle(r)(c)
    // Si no, calcula la línea y toma la columna deseada
    } else {
      auxPascal(r - triangle.size + 1, triangle.last)(c)
    }
  }

  // Tailrecursive. Calcula los valores incrementalmente
  @tailrec def auxPascal(r: Int, current: Seq[Int]): Seq[Int] = {
    // Si ya no hay mas lineas por calcular regresa la línea actual
    if (r == 0) {
      current
    } else {
      // Si no, calcula una nueva linea basada en la anterior
      val newCurrent = (current :+ 0).sliding(2)
        .foldLeft(zero)((x, y) => x :+ (y(0) + y(1)))

      // Almacena la nueva línea
      triangle += newCurrent

      auxPascal(r - 1, newCurrent)
    }
  }
}
