package interview

/* DESCRIPCIÓN DEL PROBLEMA
 *
 * Conteo de cambio. Escribe una función que cuente todas las maneras diferentes de poder dar cambio para un monto, dada una lista de denominaciones de monedas. Por ejemplo, hay tres maneras de dar cambio para el monto 4, si tienes monedas con denominación 1 y 2: 1+1+1+1, 1+1+2, 2+2.
 *   
 *   Implementa la función conteoCambio de la clase MainEjercicioCuatro.scala. Esta función toma 
 *   un monto total de cambio, y una lista única de denominaciones de monedas.
 *   Tips:   De nuevo puedes utilizar las funciones isEmpty, head y tail en la lista de enteros de 
 *     monedas
 *     Piensa el caso, ¿De cuantas formas puedes dar cambio para un monto 0?
 *     Piensa el caso,  ¿De cuantas formas puedes dar cambio para un monto > 0, si ya no 
 *     no tienes monedas?.
 */

/*
 * ::::::::::::::: SOLUCIÓN ::::::::::::::::
 * Irving Elyasib Santiago Catzim
 *
 *  La solución al problema es una forma general de la solución
 *  calculo del número fibonacci:
 *
 *  f(x) = x>2?f(x-1)+f(x-2):x
 *
 *  una solución bastante simple puede ser:
 * 
 *    def conteoCambio(monto: Int, monedas: List[Int]): Int = {
 *      if(monto == 0) {
 *        1
 *      } else if((monto > 0 && monedas.isEmpty) || monto < 0) {
 *        0
 *      } else {
 *        conteoCambio(monto - monedas.head, monedas) + conteoCambio(monto, monedas.tail)
 *      }
 *    }
 *
 *  Sin embargo una solucion con "memoization" puede ser bastante más
 *  eficiente al evitar numerosos calculos repetidos para montos grandes
 *  y/o gran cantidad de denominaciones, y reducir las probabilidades de
 *  stackoverflow usando una función recursiva
 */


object MainEjercicioCuatro {

  def conteoCambio(monto: Int, monedas: List[Int]): Int = {
    // Este mapa guarda el resultado de subramas que ya han sido exploradas
    val map = scala.collection.mutable.HashMap[(Int, Int), Int]()

    def auxCambio(monto: Int, monedas: => List[Int]): Int = {
      if(monto == 0) {
        1
      } else if((monto > 0 && monedas.isEmpty) || monto < 0) {
        0
      } else {
        val partial1 = {
          // Se obtiene el resultado de una rama precomputada si ya existe,
          // si no, se calcula y se almacena en map
          map.get(monto, monedas.size)
            .getOrElse{
              val partial = auxCambio(monto - monedas.head, monedas)
              map += ((monto, monedas.size) -> partial)
              partial
            }
        }
        val partial2 = {
          // Se obtiene el resultado de una rama precomputada si ya existe,
          // si no, se calcula y se almacena en map
          map.get(monto, monedas.size - 1)
            .getOrElse{
              val partial = auxCambio(monto, monedas.tail)
              map += ((monto, monedas.size - 1) -> partial)
              partial
            }
        }
        partial1 + partial2
      }
    }

    auxCambio(monto, monedas)
  }

}
